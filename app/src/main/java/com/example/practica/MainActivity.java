package com.example.practica;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.practica.Api.api;
import com.example.practica.Servicos.ServicioPeticion;
import com.example.practica.ViewModels.Peticion_Login;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private TextView txtRegistrar;
    private EditText edtCorreo;
    private EditText edtContraseña;
    private Button btnAceptar;
    private String APITOKEN = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtRegistrar = (TextView) findViewById(R.id.txtRegistrar);
        edtCorreo = (EditText) findViewById(R.id.edtCorreo);
        edtContraseña = (EditText) findViewById(R.id.edtContraseña);
        btnAceptar = (Button) findViewById(R.id.btnAceptar);



        txtRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent Login = new Intent(MainActivity.this, Registrar.class);
                startActivity(Login);
            }
        });

        // SE VERIFICA SI TIENE UNA SESION INICIADA

        SharedPreferences preferencias = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        String token = preferencias.getString("TOKEN", "");
        if (!token.equals("")) {
            Toast.makeText(MainActivity.this, "Bienvenido Nuevamente", Toast.LENGTH_LONG).show();
            startActivity(new Intent(MainActivity.this, MenuNoticias.class));
        }
        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ServicioPeticion service = api.getApi(MainActivity.this).create(ServicioPeticion.class);
                Call<Peticion_Login> loginCall = service.getLogin(edtCorreo.getText().toString(), edtContraseña.getText().toString());
                loginCall.enqueue(new Callback<Peticion_Login>() {
                    @Override
                    public void onResponse(Call<Peticion_Login> call, Response<Peticion_Login> response) {
                        Peticion_Login peticion = response.body();
                        if (peticion.estado.equals("true")) {

                            APITOKEN = peticion.token;
                            guardarPreferencias();
                            Toast.makeText(MainActivity.this,"Bienvenido", Toast.LENGTH_LONG).show();
                            startActivity(new Intent(MainActivity.this, MenuNoticias.class));

                        } else {
                            Toast.makeText(MainActivity.this, "Datos Incorrectos", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Peticion_Login> call, Throwable t) {
                        Toast.makeText(MainActivity.this, "Error", Toast.LENGTH_LONG).show();
                    }
                });


            }
        });
    }

    public void guardarPreferencias() {
        SharedPreferences preferencias = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        String token = APITOKEN;
        SharedPreferences.Editor editor = preferencias.edit();
        editor.putString("TOKEN", token);
        editor.commit();
    }
}

