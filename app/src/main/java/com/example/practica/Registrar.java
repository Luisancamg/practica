package com.example.practica;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.practica.Api.api;
import com.example.practica.Servicos.ServicioPeticion;
import com.example.practica.ViewModels.Registro_Usuario;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Registrar extends AppCompatActivity {

    private TextView txtLogin;
    private EditText edtUsuario;
    private EditText edtContraseña;
    private EditText edtRContraseña;
    private Button btnRegistrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrar);

        txtLogin = (TextView) findViewById(R.id.txtLogin);
        edtUsuario = (EditText) findViewById(R.id.edtCorreo);
        edtContraseña = (EditText) findViewById(R.id.edtContraseña);
        edtRContraseña = (EditText) findViewById(R.id.edtRContraseña);
        btnRegistrar = (Button) findViewById(R.id.btnRegistrar);


        txtLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent Registro = new Intent(Registrar.this, MainActivity.class);
                startActivity(Registro);
            }
        });

        btnRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(edtUsuario.getText().toString()))
                    edtUsuario.setError("Este campo no puede estar vacio");
                if (TextUtils.isEmpty(edtContraseña.getText().toString()))
                    edtContraseña.setError("Este campo no puede estar vacio");
                if (TextUtils.isEmpty(edtRContraseña.getText().toString()))
                    edtRContraseña.setError("Este campo no puede estar vacio");
                else if (!TextUtils.equals(edtContraseña.getText(), edtRContraseña.getText()))
                    edtRContraseña.setError("La Contraseña no coincide");
                else {

                    ServicioPeticion service = api.getApi(Registrar.this).create(ServicioPeticion.class);
                    Call<Registro_Usuario> registrarCall = service.registrarUsuario(edtUsuario.getText().toString(), edtContraseña.getText().toString());
                    registrarCall.enqueue(new Callback<Registro_Usuario>() {
                        @Override
                        public void onResponse(Call<Registro_Usuario> call, Response<Registro_Usuario> response) {
                            Registro_Usuario peticion = response.body();
                            if (response.body() == null) {
                                Toast.makeText(Registrar.this, "Ocurrio un error, intentalo mas tarde", Toast.LENGTH_SHORT).show();
                                return;
                            }
                            if (peticion.estado == "true") {
                                startActivity(new Intent(Registrar.this, MainActivity.class));
                                Toast.makeText(Registrar.this, "Datos Registrados", Toast.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(Registrar.this, peticion.detalle, Toast.LENGTH_LONG).show();
                            }

                        }

                        @Override
                        public void onFailure(Call<Registro_Usuario> call, Throwable t) {
                            Toast.makeText(Registrar.this, "Error", Toast.LENGTH_LONG).show();

                        }


                    });
                }
            }
        });
    }
}
